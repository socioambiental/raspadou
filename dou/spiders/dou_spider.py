import scrapy
import datetime
import json
import dataset
#import pymongo
#from pprint import pprint

class DouSpider(scrapy.Spider):
    name = "dou"

    def start_requests(self):
        # Database connection: Mongo
        #self.mongo_uri = self.settings['MONGO_URI']
        #self.mongo_db  = self.settings['MONGO_DATABASE']
        #self.client    = pymongo.MongoClient(self.mongo_uri)
        #self.db        = self.client[self.mongo_db]

        # Database connection: PostgreSQL
        #self.db    = dataset.connect(self.settings['POSTGRES_PIPELINE']['connection'])
        #self.table = self.db[self.settings['POSTGRES_PIPELINE']['table_name']]
        #self.logger.debug("Table: " + str(self.table))

        # DOU sections
        sections = getattr(self, 'sections', '1,2,3')
        sections = sections.split(',')

        # Implementation using date and days arguments
        #date   = getattr(self, 'date',     datetime.date.today().strftime('%d-%m-%Y'))
        #date   = datetime.datetime.strptime(date, '%d-%m-%Y')
        #days   = int(getattr(self, 'days', '365'))
        #deltas = [date - datetime.timedelta(days=x) for x in range(0, days)]
        #dates  = [s.strftime('%d-%m-%Y') for s in deltas]

        # Implementation using from/to dates
        date_from = getattr(self, 'from', '02-01-2013').split('-')                             # list from string, first date in the system
        date_to   = getattr(self, 'to', datetime.date.today().strftime('%d-%m-%Y')).split('-') # list from string
        date_from = datetime.date(int(date_from[2]), int(date_from[1]), int(date_from[0]))     # date
        date_to   = datetime.date(int(date_to[2]),   int(date_to[1]),   int(date_to[0]))       # date
        days      = date_to - date_from                                                        # timedelta object
        days      = int(days.total_seconds() / 86400) + 1                                      # integer (+1 is added to the days to include the first date)
        deltas    = [date_to - datetime.timedelta(days=x) for x in range(0, days)]             # list of deltas
        dates     = [s.strftime('%d-%m-%Y') for s in deltas]                                   # list of dates

        # URLs
        #urls    = [ f'http://www.in.gov.br/leiturajornal?secao=dou{section}&data={date}' for section in range(1, 4) for date in dates ]
        urls     = [ f'http://www.in.gov.br/leiturajornal?secao=dou{section}&data={date}' for section in sections for date in dates ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parseIndex)

    def parseIndex(self, response):
        inner = 'http://www.in.gov.br/web/dou/-/'
        data  = json.loads(response.xpath("//script[@id='params']/text()").get())

        #pprint.pprint(data)
        #yield data

        # See https://docs.scrapy.org/en/latest/topics/request-response.html#passing-additional-data-to-callback-functions
        for entry in data['jsonArray']:
            page                  = inner + entry['urlTitle']
            entry['date']         = datetime.datetime.strptime(data['dateUrl'], '%d-%m-%Y').strftime('%Y-%m-%d')
            entry['summary']      = entry.pop('content')
            request               = response.follow(page, callback=self.parseBody)
            request.meta['entry'] = entry;

            # Check if items are arleady stored in the db before fetching them
            #if self.table.find_one(urlTitle=entry['urlTitle']) == None:
            #    yield request
            #else:
            #    self.logger.info('Ignoring entry: %s', entry['urlTitle'])

            # Only include items that are not in the database
            #if self.db[self.name].find_one({ 'urlTitle': entry['urlTitle'] }) == None:
            #    yield request
            #else:
            #    self.logger.info('Ignoring entry: %s', entry['urlTitle'])

            yield request

    def parseBody(self, response):
        entry         = response.meta['entry']
        entry['body'] = response.css('.texto-dou').get()
        yield entry
