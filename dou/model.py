#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# RaspaDOU database model.
#
# Copyright (C) 2020 Instituto Socioambiental
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import Table, Column, Integer, String, Date, Float
from sqlalchemy                 import UniqueConstraint

Base = declarative_base()

class Dou(Base):
    """The DOU model."""

    __tablename__  = 'dou'
    #__table_args__ = (
    #                    # Attempt to add contraints, useful for an append mode
    #                    UniqueConstraint('title'),
    #                 )
    id             = Column(Integer, primary_key=True)
    artType        = Column(String)
    body           = Column(String)
    date           = Column(Date)
    hierarchyList  = Column(String)
    hierarchyStr   = Column(String)
    #numberPage    = Column(Integer)
    numberPage     = Column(String)
    pubOrder       = Column(String)
    summary        = Column(String)
    title          = Column(String)
    urlTitle       = Column(String)
    number         = Column(String)
