# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem

#
# DouPipeline
#

class DouPipeline(object):
    def process_item(self, item, spider):
        fields = [ 'artType', 'body', 'date', 'hierarchyList', 'hierarchyStr', 'numberPage',
                   'pubOrder', 'summary', 'title', 'urlTitle' ]

        for field in fields:
            if not item.get(field):
                raise DropItem(f"Missing {field} in {item}")

        return item

#
# MongoPipeline
#

import pymongo

# Works, but MongoDB has poor performance for selects
class MongoPipeline(object):
    collection_name = 'dou'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db  = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db     = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        if self.db[self.collection_name].find_one({ 'urlTitle': item.get('urlTitle') }) == None:
            self.db[self.collection_name].insert_one(dict(item))
            return item
        else:
            raise DropItem(f"Item {item} already in the database")

#
# SqlitePipeline
#

# It could be an interesting idea BUT SQLite is not recommended for heady-loaded, concurrent operations
# Inspired by:
# Author: Jay Vaughan
# Url   : https://stackoverflow.com/questions/3261858/does-anyone-have-example-code-for-a-sqlite-pipeline-in-scrapy#8733888
#
# Pipelines for processing items returned from a scrape.
# Dont forget to add pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
#
# The needed database schema:
#
#  'id'
#  'artType': varchar
#  'content': varchar
#  'hierarchyLevelSize': int
#  'hierarchyList': array
#  'hierarchyStr': varchar
#  'numberPage': int
#  'pubOrder': varchar
#  'title': varchar
#  'urlTitle': varchar
#  'body': varchar
#  'date': date

#from scrapy import log
#from pysqlite2 import dbapi2 as sqlite
#
## This pipeline takes the Item and stuffs it into scrapedata.db
#class SqLitePipeline(object):
#    def __init__(self):
#        # Possible we should be doing this in spider_open instead, but okay
#        self.connection = sqlite.connect('./scrapedata.db')
#        self.cursor = self.connection.cursor()
#        self.cursor.execute('CREATE TABLE IF NOT EXISTS myscrapedata ' \
#                    '(id INTEGER PRIMARY KEY, url VARCHAR(80), desc VARCHAR(80))')
#
#    # Take the item and put it in database - do not allow duplicates
#    def process_item(self, item, spider):
#        self.cursor.execute("select * from myscrapedata where url=?", item['url'])
#        result = self.cursor.fetchone()
#        if result:
#            log.msg("Item already in database: %s" % item, level=log.DEBUG)
#        else:
#            self.cursor.execute(
#                "insert into myscrapedata (url, desc) values (?, ?)",
#                    (item['url'][0], item['desc'][0])
#
#            self.connection.commit()
#
#            log.msg("Item stored : " % item, level=log.DEBUG)
#        return item
#
#    def handle_error(self, e):
#        log.err(e)

#
# PgPipeline
# Status: Broken: alembic migration task locks postgres
#         See https://stackoverflow.com/questions/22896496/alembic-migration-stuck-with-postgresql
#             https://stackoverflow.com/questions/45697699/flask-migrate-hangs-on-table-modificiation#50011391
#

#import logging
#import dataset
#from sqlalchemy.dialects.postgresql import JSONB
#logger = logging.getLogger(__name__)
#
#class PgPipeline(object):
#    def __init__(self, **kwargs):
#        self.args = kwargs
#
#    @classmethod
#    def from_crawler(cls, crawler):
#        args = crawler.settings.get('PG_PIPELINE', {})
#        return cls(**args)
#
#    def open_spider(self, spider):
#        if self.args.get('connection'):
#            self.db = dataset.connect(self.args.get('connection'))
#            self.table = self.db[self.args.get('table_name')]
#
#            # Somebody might explain a weird issue: without some statement
#            # evaluating self.table, the follwing error happens:
#            #
#            #   2019-06-18 19:41:54 [scrapy.core.engine] INFO: Spider closed (shutdown)
#            #   Unhandled error in Deferred:
#            #   2019-06-18 19:41:54 [twisted] CRITICAL: Unhandled error in Deferred:
#            #
#            #   Traceback (most recent call last):
#            #     File "/home/user/apps/python/venv/lib/python3.7/site-packages/scrapy/crawler.py", line 172, in crawl
#            #       return self._crawl(crawler, *args, **kwargs)
#            #     File "/home/user/apps/python/venv/lib/python3.7/site-packages/scrapy/crawler.py", line 176, in _crawl
#            #       d = crawler.crawl(*args, **kwargs)
#            #     File "/home/user/apps/python/venv/lib/python3.7/site-packages/twisted/internet/defer.py", line 1613, in unwindGenerator
#            #       return _cancellableInlineCallbacks(gen)
#            #     File "/home/user/apps/python/venv/lib/python3.7/site-packages/twisted/internet/defer.py", line 1529, in _cancellableInlineCallbacks
#            #       _inlineCallbacks(None, g, status)
#            #   --- <exception caught here> ---
#            #     File "/home/user/apps/python/venv/lib/python3.7/site-packages/twisted/internet/defer.py", line 1418, in _inlineCallbacks
#            #       result = g.send(result)
#            #     File "/home/user/apps/python/venv/lib/python3.7/site-packages/scrapy/crawler.py", line 82, in crawl
#            #       yield self.engine.open_spider(self.spider, start_requests)
#            #   dataset.util.DatasetException: Table has not been created yet.
#            #
#            #   2019-06-18 19:41:54 [twisted] CRITICAL:
#            #   Traceback (most recent call last):
#            #     File "/home/user/apps/python/venv/lib/python3.7/site-packages/twisted/internet/defer.py", line 1418, in _inlineCallbacks
#            #       result = g.send(result)
#            #     File "/home/user/apps/python/venv/lib/python3.7/site-packages/scrapy/crawler.py", line 82, in crawl
#            #       yield self.engine.open_spider(self.spider, start_requests)
#            #   dataset.util.DatasetException: Table has not been created yet.
#            #
#            # This issue prevented the use of PgPipeline from upstream and this whole class is a duplication of code.
#            logger.debug("Table: " + str(self.table))
#
#            self.pkey = self.args.get('pkey')
#            self.types = self.args.get('types', {})
#            self.ignore_identical = self.args.get('ignore_identical')
#            self.table.create_index([self.pkey])
#            self.table.create_index(self.ignore_identical)
#            self.onconflict = self.args.get('onconflict', 'ignore')
#
#            self.enabled = True
#
#    def process_item(self, item, spider):
#        if self.enabled:
#            if self.onconflict == 'ignore':
#                logger.debug("SAVE(ignore) %s", item)
#                self.table.insert_ignore(
#                    item, self.ignore_identical, types=self.types)
#            elif self.onconflict == 'upsert':
#                logger.debug("SAVE(upsert) %s", item)
#                self.table.upsert(
#                    item, self.ignore_identical, types=self.types)
#            elif self.onconflict == 'non-null':
#                logger.debug("SAVE(non-null) %s", item)
#                row, res = self.table._upsert_pre_check(
#                    item, self.ignore_identical, None)
#                selected = item
#                if res is not None:
#                    # remove keys with none value
#                    selected = dict((k, v) for k, v in item.iteritems() if v)
#                    self.table.upsert(
#                        selected, self.ignore_identical, types=self.types)
#                else:
#                    self.table.insert(
#                        selected, self.ignore_identical, types=self.types)
#            else:
#                raise Exception("no such strategy: %s" % (self.onconflict))
#
#        else:
#            logger.debug("DISABLED")
#        return item

#
# PostgreSQL pipeline
#

import logging
logger = logging.getLogger(__name__)

from dou.model import Dou

class PostgresPipeline(object):
    def __init__(self, **kwargs):
        self.args = kwargs

    @classmethod
    def from_crawler(cls, crawler):
        args = crawler.settings.get('POSTGRES_PIPELINE', {})
        return cls(**args)

    def open_spider(self, spider):
        if self.args.get('connection'):
            from sqlalchemy import create_engine

            # Create the database engine
            self.engine = create_engine(self.args.get('connection'))

            # Ensure the database has the modeled tables and cols
            Dou.metadata.create_all(self.engine)

            # Start session
            from sqlalchemy.orm import sessionmaker

            Session      = sessionmaker(bind=self.engine)
            self.session = Session()

            self.enabled = True

    def process_item(self, item, spider):
        if self.enabled:
            logger.debug("SAVE %s", item)

            import re

            #regexp = r"Nº [0-9.]*, de [0-9]{1,2} de [a-z]* de [0-9]{4}$"
            #regexp  = r"Nº [0-9.]*(/[a-z]*)*, de [0-9]{1,2} de [a-z]* de [0-9]{4}( \(\*\))*$"
            regexp  = r"Nº [0-9.]*(/[a-z]*)*, de [0-9]{1,2}º* de [a-z]* de [0-9]{4}( \(\*\))*$"
            search  = re.search(regexp, item['title'], re.IGNORECASE)
            number  = search[0] if search else ''
            article = Dou(
                    artType        = item['artType'],
                    body           = item['body'],
                    date           = item['date'],
                    hierarchyList  = item['hierarchyList'],
                    hierarchyStr   = item['hierarchyStr'],
                    numberPage     = item['numberPage'],
                    pubOrder       = item['pubOrder'],
                    summary        = item['summary'],
                    title          = item['title'],
                    urlTitle       = item['urlTitle'],
                    number         = number,
                    )

            self.session.add(article)
            self.session.commit()
        else:
            logger.debug("DISABLED")
        return item
