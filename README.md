RaspaDOU
========

Raspador do Diário Oficial da União.

Motivação
---------

O [Diário Oficial da União](http://www.in.gov.br) disponibiliza sua [base de
dados dos meses anteriores em formato XML](http://www.in.gov.br/acesso-a-informacao/dados-abertos/base-de-dados).

Mas, para o acompanhamento diário e automatizado das publicações, disponibilizamos um scraper que importa os
dados para uma base PostgreSQL ou MongoDB.

Requisitos de sistema
---------------------

Este raspador possui algumas dependências básicas, como

* Git.
* GNU Make.
* [Pipenv](https://docs.pipenv.org).
* [Banco PostgreSQL](https://www.postgresql.org/).

Estas dependências são instaladas automaticamente no sistema compatível com Debian através
do script provisionador e usando o programa `sudo`. Para tanto, instale o `make` e rode
o provisionar a partir de um usuário que possua acesso sudo:

    sudo apt install make git
    git clone https://gitlab.com/socioambiental/raspadou
    cd raspador
    make provision

<!--
    bin/provision [caminho-para-o-python-venv]
    make provision
-->

Isso irá instalar diversas ferramentas para montar uma estação de download de emergência.
Algumas delas vão pedir a senha do usuário para que possam instalar pacotes de sistema.

Se preferir, instale manualmente as dependências listadas no script `bin/provision`.

Base de dados
-------------

Certifique-se de configurar uma base de dados PostgreSQL e uma conta de acesso,
por exemplo de nome `raspadou`:

    sudo -u postgres bash
    createuser --pwprompt raspadou
    createdb  -O raspadou raspadou

Usuário ou máquina dedicados
----------------------------

Se preferir, crie uma conta de usuário específica para a atividade de download, evitando
que o mesmo tenha acesso administrativo em seu sistema, por exemplo de nome `raspadou`:

    sudo adduser raspadou

Se for este o caso, faça o login com essa conta e siga o resto das instruções após
clonar novamente o código na área deste usuário:

    git clone https://gitlab.com/socioambiental/raspadou

Outra opção consiste em usar o script numa máquina virtual ou servidor dedicado
baseado em Debian ou Ubuntu.

Dependências locais
-------------------

Em seguida, instale as dependências locais do repositório:

    make vendor

Raspando
--------

Crie um arquivo `local.py` na pasta do repositório contendo as credenciais da
base de dados:

    psql = 'postgresql://raspadou:PASSWORD@localhost:5432/raspadou'

Inicie o scraping:

    make dou

Por padrão, o scraper importa todas as entradas de todas as seções do jornal
desde a primeira data disponível na versão web do Diário Oficial até a data
atual. A versão web não contém todas as edições do DOU disponíveis como na
versão PDF.

O script de raspagem também pode ser chamado diretamente via linha de comando
através da seguinte sintaxe:

    pipenv run scrapy crawl dou [-a from=DATA_INICIAL] [-a to=DATA_FINAL] [-a sections=SECOES]

Onde:

* Datas (`from` e `to`) são aceitas no formato dia-mês-ano (`%d-%m-%Y`).
* Seções (`sections`) do Diário Oficial são separadas por vírgula, por exemplo `1,2,3` ou somente `3`.

Como exemplos, datas e as seções podem ser customizadas facilmente via linha de
comando chamando o script diretamente:

    pipenv run scrapy crawl dou -a from=01-01-2014
    pipenv run scrapy crawl dou -a to=01-10-2014
    pipenv run scrapy crawl dou -a from=01-01-2014 -a to=01-10-2014
    pipenv run scrapy crawl dou -a from=01-01-2014 -a to=01-10-2014 -a sections=1,3
    pipenv run scrapy crawl dou -a sections=1,3

O progresso da raspagem pode ser acompanhado com o seguinte comando:

    make logs

Se preferir, use o console (GNU Screen) que ativa a raspagem e monitora os logs:

    make console

Referências
-----------

* [Scrapy Docs](https://docs.scrapy.org).
* [Scrapyd](https://github.com/scrapy/scrapyd) ([docs](https://scrapyd.readthedocs.io/en/stable/index.html)).

Projetos similares
------------------

* [Nosso Querido Diário Oficial](https://github.com/okfn-brasil/queriDO).
* [Trazdia, o trazedor de Diários Oficiais. Ele baixa os Diários Oficiais brasileiros direto dos sites do governo.](https://github.com/okfn-brasil/trazdia).

Licença de Uso
--------------

Este projeto é duplamente licenciado em CC-BY-4.0 e GPLv3. As licenças estão disponíveis na pasta `licenses`.
