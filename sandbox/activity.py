#/usr/bin/env python3
#
# This sandbox script helps both to test the database and to
# produce some metrics to evaluate public government activities.
#

# Dates used in the analysis
# See https://pt.wikipedia.org/wiki/Lista_de_presidentes_do_Brasil
#     https://pt.wikipedia.org/wiki/Governo_Dilma_Rousseff
dates = {
        # Second Dilma mandate until impeachment
        'dilma': {
            'initial' : '20150101',
            'final'   : '20160831',
            'selector': "date >= '2015-01-01' and date < '2016-09-01'",
            },
        # Temer mandate right after the impeachment
        'temer' {
            'initial' : '20160901',
            'final'   : '20181231',
            'selector': "date >= '2016-09-01' and date < '2019-01-01'",
            },
        # Bolsonaro mandate until an arbitrary date in the dataset
        'bolsonaro' {
            'initial' : '20190101',
            'final'   : '20201007',
            'selector': "date >= '2019-01-01'",
            },
        }

# Average body size in kB
# select avg(octet_length(body)/1024) from dou where date

# Total articles published
# select count(title) from dou
