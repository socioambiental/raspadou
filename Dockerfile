FROM docker.io/ubuntu:focal AS raspadou
MAINTAINER Silvio <silvio@socioambiental.org>

ENV APP="${COMPOSE_PROJECT_NAME:-raspadou}"
ENV APP_BASE="/var/sites/"
ENV SHELL="/bin/bash"

# UID and GID might be read-only values, so use non-conflicting ones
ARG CONTAINER_UID="${CONTAINER_UID:-1000}"
ARG CONTAINER_GID="${CONTAINER_GID:-1000}"

WORKDIR ${APP_BASE}/${APP}

ENTRYPOINT [ "/bin/pipenv", "shell" ]

ARG DEBIAN_FRONTEND=noninteractive
RUN mkdir -p ${APP_BASE}/${APP} /home/${APP}/.ssh

# Slower approach, root user
#COPY . ${APP_BASE}/${APP}
#RUN  ${APP_BASE}/${APP}/bin/provision
#RUN  rm -rf /var/lib/apt/lists/*

# Faster approach, regular user (but replicates bin/provision)
# See https://buddy.works/guides/how-speed-up-docker-build
RUN apt-get update && \
    apt-get install -y \
    --no-install-recommends \
    make \
    python3-virtualenv \
    python3-pip \
    python3-dev \
    ipython3 \
    pipenv \
    postgresql \
    postgresql-server-dev-all \
    build-essential \
    libssl-dev \
    zlib1g-dev \
    libbz2-dev \
    libreadline-dev \
    libsqlite3-dev \
    libffi-dev \
    libxml2-dev \
    libxslt1-dev \
    libre2-dev \
    pkg-config \
    && rm -rf /var/lib/apt/lists/*

# Switch to a regular user
RUN groupadd -r -g ${CONTAINER_GID} ${APP} && \
    useradd --no-log-init -r -u ${CONTAINER_UID} -g ${APP} ${APP} && \
    mkdir -p /home/${APP} && chown ${APP}. /home/${APP}
COPY --chown=${APP}:${APP} . ${APP_BASE}/${APP}
USER ${APP}

# Vendorize
RUN make pipenv
